# OLAP Krychle s použitím npm balíčku olap-cube-js (ISC)
# UJEP - KI/DUL - Zápočtová práce


## git clone \<url projektu>
## cd \<nazev složky>
## git checkout dev
## npm i (nainstaluje vše potřebné z package.json)



### npm **start** - spustí hot reload (není potřeba furt spouštět server, reaguje na změny v kódu ve složce /src)
### npm **exec** - spustí jednorázově

- src/index.ts - entry point (vše se spouští z něj)
- build - generované .js (nemodifikovat)
- dokumentace olap-cube-js https://www.npmjs.com/package/olap-cube-js
