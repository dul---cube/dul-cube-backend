import * as Type from './types';
export const facts: string[] = ['id', 'num', 'name', 'type', 'candy', 'candy_count', 'egg', 'weaknesses', 'next_evolution', 'prev_evolution', 'weight']
export const dimensionTables: Array<Type.DimensionTable> = [
    {
        dimensionTable: {
            dimension: 'candy',
            keyProps: ['candy'],
        },
    },
    {
        dimensionTable: {
            dimension: 'candy_count',
            keyProps: ['candy_count'],
        },
    },
    {
        dimensionTable: {
            dimension: 'egg',
            keyProps: ['egg'],
        }
    },
    {
        dimensionTable: {
            dimension: 'type',
            keyProps: ['type'],
        },
        level: [
            {
                dimensionTable: {
                    dimension: 'weaknesses',
                    keyProps: ['weaknesses'],
                }
            }
        ]
    },
    {
        dimensionTable: {
            dimension: 'name',
            keyProps: ['name'],
            otherProps: ['num', 'next_evolution', 'prev_evolution'],
        }
    }
]
