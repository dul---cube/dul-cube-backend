const Cube = require('olap-cube-js');
const fetch = require('node-fetch');

import * as Type from './types';
import * as Schema from './schema';
const getData = (url: string) => {
    return fetch(url, { method: 'GET' });
}

/*

  id: 100,
      num: '100',
      name: 'Voltorb',
      img: 'http://www.serebii.net/pokemongo/pokemon/100.png',
      type: [Array],
      height: '0.51 m',
      weight: '10.4 kg',
      candy: 'Voltorb Candy',
      candy_count: 50,
      egg: '5 km',
      spawn_chance: 0.65,
      avg_spawns: 65,   
      spawn_time: '04:36',
      multipliers: [Array],
      weaknesses: [Array],
      next_evolution: [Array]

 */

class Cuba {
    public static setFacts = (facts: Array<string>, data: Array<Type.Pokemon>): Array<Type.Pokemon> => {
        return data.map((p: any, i) => {
            const temp: any = {};
            facts.forEach((f: string) => {
                if (p[f]) {
                    if (typeof p[f] === 'object' && p[f].length >= 0) {
                        temp[f] = p[f][0];
                        if (typeof p[f][0] === 'object') {
                            temp[f] = p[f][0][Object.keys(p[f][0])[0]]
                        }
                    }
                    else {
                        if (typeof p[f] === 'string' && p[f].includes('kg')) {
                            temp[f] = parseFloat(p[f]);
                        }
                        else {
                            temp[f] = p[f];
                        }
                    }
                }
            })
            return temp;
        })
    }
    public static setDimension = (dimensionTables: Array<Type.DimensionTable>): Array<Type.DimensionTable> => {
        return dimensionTables;
    }
    public static dataDecorator = async (): Promise<Type.RootResponse> => {
        return new Promise(async (resolve, reject) => {
            const data = await getData('https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json')
            const { pokemon }: Type.Vracec = await data.json();
            const facts = Cuba.setFacts(Schema.facts, pokemon);

            const dimensionHierarchies = Cuba.setDimension(Schema.dimensionTables);

            let cube = new Cube({ dimensionHierarchies });
            cube.addFacts(facts);

            const dimensions: Array<Type.DimensionResult> = dimensionHierarchies.map((d: Type.DimensionTable) => {
                //dimensions
                const lvls: any = d.level?.map((level: Type.DimensionTable) => {
                    console.log(level)
                    return { name: level.dimensionTable.dimension, table: cube.getDimensionMembers(level.dimensionTable.dimension) }
                })
                let fields: any = {};
                cube.getDimensionMembers(d.dimensionTable.dimension).forEach((d: any) => {
                    Object.keys(d).forEach(a => {
                        if (d[a])
                            fields[a] = ""
                    })
                })
                return {
                    name: d.dimensionTable.dimension,
                    table: cube.getDimensionMembers(d.dimensionTable.dimension),
                    fields: [...Object.keys(fields)],
                    level: typeof lvls === 'object' ? lvls : undefined
                }
            })
            resolve({
                cube,
                dimensions,
                facts: cube.getFacts(),
                cells: cube.getCells(),
            })
        })

    }
    public static getInitData = (): Promise<Type.RootResponse> => {
        return new Promise(async (resolve, reject) => {
            try {
                const data = await Cuba.dataDecorator();
                resolve({
                    dimensions: data.dimensions,
                    facts: data.facts,
                    cells: data.cells
                });
            } catch (error) {
                reject(error);
            }
        })
    }
    public static setDice = async (dim: Array<Type.Dice>): Promise<Type.DiceResponse> => {
        return new Promise(async (resolve, reject) => {
            try {
                const { dimensions, facts, cube } = await Cuba.dataDecorator();
                const set: any = {};
                dim.forEach(d => {
                    const ids = d.ids.map(t => {
                        return {
                            id: t
                        }
                    })
                    set[d.name] = ids;
                })
                console.log(set)
                resolve({
                    facts: cube.dice(set).getFacts(),
                    cells: cube.dice(set).getCells(),
                });
            } catch (error) {
                reject(error);
            }


        })
    }
}

export const Factory = (data: Array<Type.Dice> = []): any => ({
    getInitData: Cuba.getInitData(),
    setDice: Cuba.setDice(data),
})