
export interface Vracec {
    pokemon: Array<Pokemon>
}
export interface Pokemon {
    id: number,
    num: string,
    name: string,
    img?: string,
    type: Array<string>,
    height?: string,
    weight?: string,
    candy?: string,
    candy_count?: number,
    egg?: string,
    spawn_chance?: number,
    avg_spawns?: number,
    spawn_time?: string,
    multipliers?: Array<number>,
    weaknesses?: Array<string>,
    next_evolution?: Array<Evolution>,
    prev_evolution?: Array<Evolution>
}

export interface Evolution {
    num: string,
    name: string
}

export interface DimensionTable {
    dimensionTable: Dimension,
    fields?: Array<any>,
    level?: Array<DimensionTable>
}
//
export interface Dimension {
    dimension: string,
    keyProps: Array<string>,
    otherProps?: Array<string>,
}

export interface DimensionResult {
    name: string,
    table: Array<DimensionTable>,
    fields: Array<string>,
    level?: Level
}
export interface Level {
    name: string,
    table: Array<DimensionTable>
}

export interface RootResponse {
    cube?: any,
    facts?: Array<Pokemon>,
    dimensions?: Array<DimensionResult>
    cells?: Array<any>
}

export interface FactoryResponse {
    getInitData: Promise<RootResponse>,
    setDice: Promise<any>
}

export interface Dice {
    name: string,
    ids: Array<number>
}

export interface DiceResponse {
    facts: Array<Pokemon>,
    cells: Array<any>,
    dimensions?: Array<DimensionResult>
}
