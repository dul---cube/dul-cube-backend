import express from 'express';
import * as Cube from './CubeController';
import * as Type from './types';
import bodyParser, { raw } from 'body-parser';
const cors = require('cors');
const app = express();
const PORT: number = 3000;

app.use(bodyParser.json());
app.use(cors())


app.get('/', async (req: express.Request, res: express.Response) => {
    try {
        const resp = await Cube.Factory().getInitData;
        res.json(resp);
    } catch (error) {
        res.status(500).json({ msg: "TY HACKUJES", todo: "NEROP" })
    }
})

app.get('/raw', async (req: express.Request, res: express.Response) => {
    try {
        const { facts } = await Cube.Factory().getInitData;
        res.json(facts);
    } catch (error) {
        res.status(500).json({ msg: "TY HACKUJES", todo: "NEROP" })
    }
})

app.get('/facts', async (req: express.Request, res: express.Response) => {
    try {
        const { cells } = await Cube.Factory().getInitData;
        res.json(cells);
    } catch (error) {
        res.status(500).json({ msg: "TY HACKUJES", todo: "NEROP" })
    }
})

app.get('/dimensions', async (req: express.Request, res: express.Response) => {
    try {
        const { dimensions } = await Cube.Factory().getInitData;
        res.json(dimensions);
    } catch (error) {
        res.status(500).json({ msg: "TY HACKUJES", todo: "NEROP" })
    }
})

app.post('/dice', async (req: express.Request, res: express.Response) => {
    try {
        const data: Array<Type.Dice> = req.body;
        console.log(req.body)
        console.log(data)
        const resp: Type.DiceResponse = await Cube.Factory(data).setDice;
        res.json(resp);
    } catch (error) {
        res.status(500).json({ msg: "TY HACKUJES", todo: "NEROP" })
    }
})

app.listen(PORT, () => {
    console.log(`Server listening to port ${PORT}`);
})